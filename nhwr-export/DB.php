<?php
require_once "config.php";
class DB {
	
	protected $db_name = DB_NAME;
	protected $db_user = DB_USER;
	protected $db_pass = DB_PASS;
	protected $db_host = DB_HOST;
	public $conn = null;
	// Open a connect to the database.
	// Make sure this is called on every page that needs to use the database.
	
	public function connect() {
	
		$this->conn = new mysqli( $this->db_host, $this->db_user, $this->db_pass, $this->db_name );
		
		if ( mysqli_connect_errno() ) {
			printf("Connection failed: %s", mysqli_connect_error());
			exit();
		}
		return true;
		
	}

	public function close(){
		$this->conn->close();
	}

	function multi_query($sql){
		return $this->conn->multi_query($sql);
	}

	static function timestampToDate($data){
		if($data !=  null && is_numeric($data)){
			return gmdate("'Y-m-d H:i:s'", $data);
		}
		return "NULL";
	}

	static function dbNumber($data){
		if($data !=  null && is_numeric($data)){
			return $data;
		}
		return "NULL";
	}

}