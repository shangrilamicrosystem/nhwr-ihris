<?php
require_once "form_base.php";

class form_person_contact_personal extends form_base{
	function __construct()
	{
		$this->form = "person_contact_personal";
		$this->transpose = false;
	}

	function prepareSql($parent, $param){
		if(strlen($parent) == 0)
			return null;
		$sql = "
SELECT 
	c.telephone personal_telephone, c.mobile_phone personal_mobile_phone, c.alt_telephone personal_alt_telephone, c.email personal_email
FROM hippo_person p 
	LEFT JOIN hippo_person_contact_personal c on p.id = c.parent
WHERE p.id = '$parent'
";
		return $sql;
	}
}