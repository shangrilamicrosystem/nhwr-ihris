<?php
require_once "form_base.php";

class form_person_parent_info extends form_base{
	function __construct()
	{
		$this->form = "person_parent_info";
		$this->parent_form = "person";
	}

	function prepareSql($parent, $param){
		if(strlen($parent) == 0)
			return null;
		$p_arr = explode("|", $parent);
		var_dump($p_arr);
		$parent_id = $p_arr[1];
		$sql = "
SELECT 
	fld.name, fld.type,
	CASE 
		WHEN fld.type = 'string' then e.string_value
		WHEN fld.type = 'integer' then e.integer_value
		WHEN fld.type = 'text' then e.text_value
		WHEN fld.type = 'date' then e.date_value
	end value
FROM form_field ff
	INNER JOIN form f on ff.form = f.id
	INNER JOIN field fld on ff.field = fld.id
	LEFT JOIN record r on f.id = r.form and parent_form = '$this->parent_form' AND r.parent_id = $parent_id
	LEFT JOIN last_entry e on ff.id = e.form_field and e.record = r.id 
	LEFT JOIN field_sequence fs on ff.id = fs.form_field
WHERE f.name = '$this->form'
ORDER BY fs.sequence";
		return $sql;
	}

}