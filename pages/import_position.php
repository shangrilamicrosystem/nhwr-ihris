#!/usr/bin/php
<?php
/*
 * © Copyright 2007, 2008 IntraHealth International, Inc.
 *
 * This File is part of iHRIS
 *
 * iHRIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The page wrangler
 *
 * This page loads the main HTML template for the home page of the site.
 * @package iHRIS
 * @subpackage DemoManage
 * @access public
 * @author Sovello Hildebrand sovellohpmgani@gmail.com
 * @copyright Copyright &copy; 2007, 2008-2013 IntraHealth International, Inc.
 * @version 4.6.0
 */
/*
php import_data.php /path/to/your/excel_sheet.csv
*/
require_once("./import_base.php");

class PersonalData_Import extends Processor{

		public function __construct($file) {
				parent::__construct($file);
		}

		//map headers from the spreadsheet
		//what you do here is change the values on the right to match what you have on the spreadsheet. comment out lines that are not in the spreadsheet
		//the values of the left are used by the script to refer to the spreadsheet columns on the right of this array.
		//the order of the columns in the spreadsheet doesn't matter
		//{"job":"job","title":"title","pos_type":"pos_type","facility":"facility"}
		protected function getExpectedHeaders(){
			return array(
				"job" => "job",
				"title" => "title",
				"pos_type" => "pos_type",
				"facility" => "facility",
				
				// 'surname' => 'surname',
				// 'firstname' => 'firstname',
				// 'middle_name' => 'middle_name',
				// 'nationality' => 'nationality',
				// 'residence' => 'residence',
				// 'person_parent_info' => 'person_parent_info',
				// 'demographic' => 'demographic',
				// 'person_id' => 'person_id',
				// 'registration' => 'registration',
				// 'person_contact_personal' => 'person_contact_personal',
				//'person_contact_emergency' => 'person_contact_emergency'
				// 'district' => 'facilitylocation',
				// 'birth_date' => 'dateofbirth',
				// 'gender' => 'sex',
				// 'marital_status' => 'maritalstatus',
				// 'id_num' => 'employeenumber',
				// 'othername' => 'middlename',
				// 'maiden' => 'maidenname',
				//'nxtofkin_name' => 'Next of Kin Name',
				//'nxtofkin_address' => 'Next of kin address',
				//'nxtofkin_relationship' => 'relationship of Next of kin',
				//'nxtofkin_email' => 'next of kin email',
				//'nxtofkin_mobile_phone' => 'next of kin mobile',
				//'nxtofkin_telephone' => 'next of kin tel',
			);
		}

		/**
		 * this function returns the id numbers 'id_number' and id type 'id_type' for that id number.
		 *
		 */

		public function getIdNumberArray(){
			//when in the spreadsheet you have multiple columns that refer to identification numbers then you have this array to handle just that
			//with the columns set above, here you come map the columns to their specific id_type.
			//so if you have say just one column with identification numbers then comment lines 80 to 87 of this file by typing /* on line 79
			//and */ on line 89
			//change the the value in id_type to be e.g. National ID to match the type of identification for this column
			return $id_numbers = array(
				0 => array(
						'id_number' => $this->mapped_data['id_num'],
						'id_type' => 'Payroll Number'
					),
				/*
				1 => array(
						'id_number' => $this->mapped_data['id_num1'],
						'id_type' => 'Salary Number'
					),
				2 => array(
					 'id_number' => $this->mapped_data['id_num'],
						'id_type' => 'Payroll Number'
				*/

			);
		}

		//this part checks to see if the nationality/country column for a record is empty it defaults to Tanzania.
		//change this to your country. otherwise it takes the value of the country in that column
		public function getNationality(){
			return empty($this->mapped_data['nationality']) ? 'Nepal' : $this->mapped_data['nationality'];
		}

		//in this part comment out if you are not adding any data for that specific item.
		//for example if there is no nextofkin data in the spreadsheet,
		//comment out line 108 by preceding it with double-slasses as in this line
		//remember to also comment out all lines in the getExpectedHeaders() function lines 46-63
		protected function _processRow(){
			$reg_data = $this->mapped_data["registration"][0];
			$personId = $this->findPersonByReg($reg_data["council"], $reg_data["registration_number"], $reg_data["registration_date"]);
			I2CE::raiseMessage("personId = $personId");
			// $personObj = $this->ff->createContainer($personId);
			//start updating the child data
			$this->updatePersonRegistration($personId, $this->mapped_data["registration"]);
			$this->updatePersonIds($personId, $this->mapped_data['person_id']);
			$this->updateDemographic($personId, $this->mapped_data['demographic']);
			$this->updateParentInfo($personId, $this->mapped_data['person_parent_info']);
			$this->updatePersonalContact($personId, $this->mapped_data["person_contact_personal"]);
			// $this->updateEmergencyContact($personId, $this->mapped_data["person_contact_emergency"]);
			//$personId = $this->findPersonByNames($this->mapped_data['surname'], $this->mapped_data['othername'],
			//				$this->mapped_data['firstname'], $this->getNationality() );
			// $this->createPersonId($personId,$this->getIdNumberArray());
			// $this->addDemographic($personId, $this->mapped_data['birth_date'], $this->getGender($this->mapped_data['gender']),
			// 		$this->maritalStatusExists($this->mapped_data['marital_status']) );
			//$this->addNextOfKin($personId, $this->mapped_data['nxtofkin_name'],$this->mapped_data['nxtofkin_address'],$this->mapped_data['nxtofkin_relationship'],$this->mapped_data['nxtofkin_mobile']);
			//$this->addNextOfKin($personId, $this->mapped_data['nxtofkin_name'],$this->mapped_data['nxtofkin_address'],$this->mapped_data['nxtofkin_relationship'],$this->mapped_data['nxtofkin_mobile']);
			//$this->addNextOfKin($personId, $this->mapped_data['nxtofkin_name'],$this->mapped_data['nxtofkin_address'],$this->mapped_data['nxtofkin_relationship'],$this->mapped_data['nxtofkin_mobile']);
		}

		protected function updatePersonIds($personId, $Ids = array() ){
			if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
				foreach( $Ids as $index=>$data){
					$formObj = $this->ff->createContainer('person_id');
					$formObj->getField('id_num')->setValue(trim($data['id_num']));
					$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
					$formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
					$formObj->getField('place')->setValue(trim($data['place']['district']));
					// $formObj->getField('place')->setValue(array('district', $this->districtExists($data['place']['district']) ) );
					$formObj->setParent($personId);
					$this->save($formObj);
				}
			}
		}
		protected function updatePersonRegistration($personId, $registration = array() ){
			if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
				foreach( $registration as $index=>$data){
					$formObj = $this->ff->createContainer('registration');
					// var_dump($data);
					$formObj->getField('registration_number')->setValue(trim($data['registration_number']));
					$formObj->getField('registration_date')->setValue(I2CE_Date::fromDB(trim($data['registration_date'])));
					$formObj->getField('council')->setValue(array('council', $this->councilExists($data['council']) ) );
					var_dump($formObj->getField('registration_date')->getValue());
					// $formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
					// $formObj->getField('place')->setValue(array('district', $this->districtExists($data['place']['district']) ) );
					$formObj->setParent($personId);
					$this->save($formObj);
				}
			}
		}

		protected function updateParentInfo($personId, $parentDetail){
			if($personId)
			$where = array(
				'operator' => 'FIELD_LIMIT',
				'field' => 'parent',
				'style' => 'lowerequals',
				'data' => array(
						'value' => $personId
					)
				);

			$parentId = I2CE_FormStorage::search('person_parent_info', false, $where );
			if( count($parentId) == 0){
				$formObj = $this->ff->createContainer('person_parent_info');
			}
			else
				$formObj = $this->ff->createContainer($parentId);
			$formObj->getField('father_surname')->setValue(trim($parentDetail['father_surname']));
			$formObj->getField('father_firstname')->setValue(trim($parentDetail['father_firstname']));
			$formObj->getField('father_middlename')->setValue(trim($parentDetail['father_middlename']));
			$formObj->getField('mother_surname')->setValue(trim($parentDetail['mother_surname']));
			$formObj->getField('mother_firstname')->setValue(trim($parentDetail['mother_firstname']));
			$formObj->getField('mother_middlename')->setValue(trim($parentDetail['mother_middlename']));
			$formObj->getField('husband_surname')->setValue(trim($parentDetail['husband_surname']));
			$formObj->getField('husband_firstname')->setValue(trim($parentDetail['husband_firstname']));
			$formObj->getField('husband_middlename')->setValue(trim($parentDetail['husband_middlename']));

			$formObj->setParent($personId);
			$this->save($formObj);
		}


		protected function updatePersonalContact($personId, $contact){
			if($personId)
			$where = array(
				'operator' => 'FIELD_LIMIT',
				'field' => 'parent',
				'style' => 'lowerequals',
				'data' => array(
						'value' => $personId
					)
				);

			$parentId = I2CE_FormStorage::search('person_contact_personal', false, $where );
			if( count($parentId) == 0){
				$formObj = $this->ff->createContainer('person_contact_personal');
			}
			else
				$formObj = $this->ff->createContainer($parentId);
			$formObj->getField('telephone')->setValue(trim($contact['telephone']));
			$formObj->getField('mobile_phone')->setValue(trim($contact['mobile_phone']));
			$formObj->getField('alt_telephone')->setValue(trim($contact['alt_telephone']));
			$formObj->getField('email')->setValue(trim($contact['email']));
			$formObj->setParent($personId);
			$this->save($formObj);
		}

		protected function updateEmergencyContact($personId, $contact){
			if($personId)
			$where = array(
				'operator' => 'FIELD_LIMIT',
				'field' => 'parent',
				'style' => 'lowerequals',
				'data' => array(
						'value' => $personId
					)
				);

			$parentId = I2CE_FormStorage::search('person_contact_emergency', false, $where );
			if( count($parentId) == 0){
				$formObj = $this->ff->createContainer('person_contact_emergency');
			}
			else
				$formObj = $this->ff->createContainer($parentId);
			$formObj->getField('surname')->setValue(trim($contact['surname']));
			$formObj->getField('middlename')->setValue(trim($contact['middlename']));
			$formObj->getField('firstname')->setValue(trim($contact['firstname']));
			$formObj->getField('telephone')->setValue(trim($contact['telephone']));
			$formObj->getField('mobile_phone')->setValue(trim($contact['mobile_phone']));
			$formObj->getField('alt_telephone')->setValue(trim($contact['alt_telephone']));
			$formObj->getField('email')->setValue(trim($contact['email']));
			$formObj->setParent($personId);
			$this->save($formObj);
		}
		/****************************************************************************
		 *                                                                          *
		 *   DON'T EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU WANT TO ACHIEVE  *
		 *                                                                          *
		 ****************************************************************************/
		protected function createPersonId($personId, $id_nums = array() ){
			if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
				foreach( $id_nums as $index=>$data){
					$formObj = $this->ff->createContainer('person_id');
					$formObj->getField('id_num')->setValue(trim($data['id_number']));
					$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
					$formObj->setParent($personId);
					$this->save($formObj);
				}
			}
		}

		protected function updateDemographic($personId, $demo){
			if($personId)
			$where = array(
				'operator' => 'FIELD_LIMIT',
				'field' => 'parent',
				'style' => 'lowerequals',
				'data' => array(
						'value' => $personId
					)
				);

			$demoId = I2CE_FormStorage::search('demographic', false, $where );
			I2CE::raiseMessage("personId = $personId, demoId count ".count($demoId));
			if( count($demoId) == 0){
				$formObj = $this->ff->createContainer('demographic');
			}
			else
				$formObj = $this->ff->createContainer($demoId);

			$formObj->getField('birth_date')->setValue(I2CE_Date::fromDB(trim($demo['birth_date'])));
			$formObj->getField('gender')->setValue( array('gender', $this->getGender($demo['gender'])));
			// $formObj->getField('marital_status')->setValue( array('marital_status', $this->maritalStatusExists($demo['marital_status'])) );
			$co = $this->countryExists($demo['citizenship_at_birth']);
			$formObj->getField('nationality_at_birth')->setValue( array('country', $co));
			$formObj->getField('birth_location')->setValue(array('district', $this->districtExists($demo['brith_location']['district'])));
			$formObj->setParent($personId);
			$this->save($formObj);
		}
		public function checkNameExists($formName, $name){
			$name_t = strtolower(trim($name));
			$where = array(
				'operator'=>'FIELD_LIMIT',
				'field'=>'name',
				'style'=>'lowerequals',
				'data'=>array(
						'value' => $name_t
					)
			);
			if(strlen($name_t) > 0)
				$items = I2CE_FormStorage::search($formName, false, $where);
			else{
				return null;
			}
			if(count($items) >= 1 ){
				return current($items); //like 123432
			}
			elseif(count($items) == 0){
				$formObj = $this->ff->createContainer($formName);
				$formObj->getField('name')->setValue($name);
				$id = $this->save($formObj);
				return $id; //like 123432
			}
		}
		public function countryExists($name){
			return $this->checkNameExists('country', $name);
		}

		public function idTypeExists($name){
			return $this->checkNameExists('id_type', $name);
		}

		public function councilExists($name){
			return $this->checkNameExists('council', $name);
		}
		public function districtExists($name){
			return $this->checkNameExists('district', $name);
		}

		public function maritalStatusExists($name){
			return $this->checkNameExists('marital_status', $name);
		}

		public function getGender( $gender ){
			$g = strtolower(trim($gender));
			if( ($g == 'f') || ($g == 'female') || (strpos($gender,'f') == 0) ){
				return 'F';
			}
			if( ($g == 'm') || ($g == 'male') || (strpos($gender,'m') == 0) ){
				return 'M';
			}
		}

		public function findPersonByNames($surname, $firstname, $othername, $country){
			//search person by names
			$wherePerson = array(
				'operator'=>'AND',
				'operand'=>array(
					0=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'surname',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim($surname)
									)
							),
					1=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'firstname',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim($firstname)
									)
							),
					2=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'othername',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim($othername)
									)
							),
					)
				);

			$person = I2CE_FormStorage::search('person', false, $wherePerson);
			if( count($person) > 1 ){
				$this->addBadRecord("This person already exists in duplicates");
				return false;
			}
			elseif(count($person) == 1){
				return 'person|'.$person[0];
			}
			elseif( (count($person) == 0) ){
				//we need to create this person
				$personObj = $this->ff->createContainer('person');
				$personObj->firstname = trim($firstname);
				$personObj->othername = trim($othername);
				$personObj->surname = trim($surname);
				$personObj->nationality = array('country', $this->countryExists($country) );
				$personId = $this->save($personObj);
				return 'person|'.$personId;
			}
		}
		public function findPersonByReg($council, $registration_number, $registration_date){
			if($council && $registration_number){
				$council = $this->councilExists($council);
				//search person by identification
				$whereIdentification = array(
					'operator'=>'AND',
					'operand'=>array(
							0=>array(
									'operator'=>'FIELD_LIMIT',
									'field'=>'council',
									'style'=>'equals',
									'data'=>array(
											'value'=>trim($council)
											)
									),
							1=>array(
									'operator'=>'FIELD_LIMIT',
									'field'=>'registration_number',
									'style'=>'equals',
									'data'=>array(
											'value'=>trim($registration_number)
											)
									),
							2=>array(
									'operator'=>'FIELD_LIMIT',
									'field'=>'registration_date',
									'style'=>'equals',
									'data'=>array(
											'value'=>trim($registration_date)
										)
									)
							)
					);
				$person_ids = I2CE_FormStorage::listFields('registration', array('parent'), false, $whereIdentification);
				if( ( count($person_ids) == 0 )  ){
					$dt = $this->mapped_data;
					//we need to create this person
					$personObj = $this->ff->createContainer('person');
					$personObj->firstname = trim($dt["firstname"]);
					$personObj->middle_name = trim($dt["middle_name"]);
					$personObj->surname = trim($dt["surname"]);
					$personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
					$personObj->residence = array('district', trim($this->districtExists($dt['residence']['district'])));
					$personId = $this->save($personObj);
					return 'person|'.$personId;
				}
				else{
					$data = current($person_ids);
					return $data['parent'];
				}
			}
		}
		public function findPersonByID($id_number, $id_type){
			if($id_number && $id_type){
				//search person by identification
				$whereIdentification = array(
					'operator'=>'AND',
					'operand'=>array(
							0=>array(
									'operator'=>'FIELD_LIMIT',
									'field'=>'id_number',
									'style'=>'equals',
									'data'=>array(
											'value'=>trim($id_number)
											)
									),
							1=>array(
									'operator'=>'FIELD_LIMIT',
									'field'=>'id_type',
									'style'=>'equals',
									'data'=>array(
											'value'=>trim($id_type)
											)
									)
							)
					);
				$person_ids = I2CE_FormStorage::listFields('person_id', array('parent'), false, $whereIdentification);
				if( ( count($person_ids) == 0 )  ){
					return false;
				}
				else{
					$data = current($person_ids);
					return $data['parent'];
				}
			}
		}


		protected function addNextOfKin($personId, $name, $address, $relationship, $mobile ){
			if( $personId )
				$where = array(
					'operator' => 'FIELD_LIMIT',
					'field' => 'parent',
					'style' => 'lowerequals',
					'data' => array(
							'value' => $personId
						)
					);

			$nxtofkin = I2CE_FormStorage::search('nextofkin', false, $where );
			if( count($nxtofkin) == 0){
				$formObj = $this->ff->createContainer('nextofkin');
				$formObj->getField('name')->setValue( $this->mapped_data['nxtofkin_name'] );
				$formObj->getField('relationship')->setValue( $this->mapped_data['nxtofkin_relationship'] );
				$formObj->getField('telephone')->setValue( $this->mapped_data['nxtofkin_telephone'] );
				$formObj->getField('address')->setValue( $this->mapped_data['nxtofkin_address'] );
				$formObj->getField('mobile_phone')->setValue( $this->mapped_data['nxtofkin_mobile_phone'] );
				$formObj->getField('email')->setValue( $this->mapped_data['nxtofkin_email'] );
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}

}


/*********************************************
*
*      Execute!
*
*********************************************/

//ini_set('memory_limit','3000MB');


if (count($arg_files) != 1) {
		usage("Please specify the name of a spreadsheet to process");
}

reset($arg_files);
$file = current($arg_files);
if($file[0] == '/') {
		$file = realpath($file);
} else {
		$file = realpath($dir. '/' . $file);
}
if (!is_readable($file)) {
		usage("Please specify the name of a spreadsheet to import: " . $file . " is not readable");
}

I2CE::raiseMessage("Loading from $file");


$processor = new PersonalData_Import($file);
$processor->run();

echo "Processing Statistics:\n";
print_r( $processor->getStats());




# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
