#!/usr/bin/php
<?php
/*
 * © Copyright 2007, 2008 IntraHealth International, Inc.
 *
 * This File is part of iHRIS
 *
 * iHRIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The page wrangler
 *
 * This page loads the main HTML template for the home page of the site.
 * @package iHRIS
 * @subpackage DemoManage
 * @access public
 * @author Sovello Hildebrand sovellohpmgani@gmail.com
 * @copyright Copyright &copy; 2007, 2008-2013 IntraHealth International, Inc.
 * @version 4.6.0
 */
/*
php import_position_sanction_count.php ./data/position/position_2000_1.json
php import_position_sanction_count.php ./data/position/position_xxxx.json
php import_data.php /path/to/your/excel_sheet.csv

*/
require_once("./import_base.php");

class PositionData_Import extends Processor{

		public function __construct($file) {
			parent::__construct($file);
		}

		//map headers from the spreadsheet
		//what you do here is change the values on the right to match what you have on the spreadsheet. comment out lines that are not in the spreadsheet
		//the values of the left are used by the script to refer to the spreadsheet columns on the right of this array.
		//the order of the columns in the spreadsheet doesn't matter
		//{"job":"job","title":"title","pos_type":"pos_type","facility":"facility"}
		protected function getExpectedHeaders(){

// 			"job":"Medical Officer",
// "title":"Medical Officer",
// "pos_type":"Permanent Full-Time",
// "facility":"Mechi Zonal hospital, Jhapa, Mechi",
// "sanction_post_count":"7"
			return array(
				"job" => "job",
				"title" => "title",
				"pos_type" => "pos_type",
				"facility" => "facility",
				"sanction_post_count" => "sanction_post_count"
			);
		}

		/**
		 * this function returns the id numbers 'id_number' and id type 'id_type' for that id number.
		 *
		 */

		public function getIdNumberArray(){
			//when in the spreadsheet you have multiple columns that refer to identification numbers then you have this array to handle just that
			//with the columns set above, here you come map the columns to their specific id_type.
			//so if you have say just one column with identification numbers then comment lines 80 to 87 of this file by typing /* on line 79
			//and */ on line 89
			//change the the value in id_type to be e.g. National ID to match the type of identification for this column
			return $id_numbers = array(
				0 => array(
						'id_number' => $this->mapped_data['id_num'],
						'id_type' => 'Payroll Number'
					),
				/*
				1 => array(
						'id_number' => $this->mapped_data['id_num1'],
						'id_type' => 'Salary Number'
					),
				2 => array(
					 'id_number' => $this->mapped_data['id_num'],
						'id_type' => 'Payroll Number'
				*/

			);
		}

		//this part checks to see if the nationality/country column for a record is empty it defaults to Tanzania.
		//change this to your country. otherwise it takes the value of the country in that column
		public function getNationality(){
			return empty($this->mapped_data['nationality']) ? 'Nepal' : $this->mapped_data['nationality'];
		}

		//in this part comment out if you are not adding any data for that specific item.
		//for example if there is no nextofkin data in the spreadsheet,
		//comment out line 108 by preceding it with double-slasses as in this line
		//remember to also comment out all lines in the getExpectedHeaders() function lines 46-63
		protected function _processRow(){
			$positionDetail = $this->mapped_data;
			$addedRows = $this->checkAndUpdateSanctionedPosition($positionDetail);
			I2CE::raiseMessage("No of records imported = $addedRows");
		}
		public function checkAndUpdateSanctionedPosition($data){
			$count = $this->getPositionCount($data);
			if($count < 1 || isset($count) == false)
				$count = 0;
			$sanctioned_count = $data["sanction_post_count"];
			if($sanctioned_count < 1 || isset($sanctioned_count) == false)
				$sanctioned_count = 0;
			if($sanctioned_count > $count){
				$jobId = $this->jobExists($data['job']);
				$facilityId = $this->facilityExists($data['facility']);
				for($i=0, $len = $sanctioned_count - $count; $i< $len; $i++ ){
					$formObj = $this->ff->createContainer('position');
					var_dump(array($jobId, $facilityId));
					$formObj->job = array('job', $jobId);
					$code = trim($data['code']);
					if(0 == strlen($code))
						$code = trim($data['title']);
					$formObj->code = $code;
					$formObj->title = trim($data['title']);
					$formObj->facility = array('facility', $facilityId);
					$formObj->status = array('position_status','open');
					$positionId = $this->save($formObj);
				}
				return $sanctioned_count - $count;
			}
			return 0;
		}
		public function getPositionCount($data, $status = "open"){
			$jobId = $this->jobExists($data['job']);
			$facilityId = $this->facilityExists($data['facility']);
			//search position by title
			$wherePosition = array(
				'operator'=>'AND',
				'operand'=>array(
					0=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'title',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim($data['title'])
									)
							),
					1=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'job',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim("job|".$jobId)
									)
							),
					2=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'facility',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim("facility|".$facilityId)
									)
							)/*,
					3=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'status',
							'style'=>'lowerequals',
							'data'=>array(
									'value'=>trim("position_status|".$status)
									)
							)*/
					)
				);
			$position = I2CE_FormStorage::search('position', false, $wherePosition);
			I2CE::raiseMessage("Position COUNT = ".count($position));
			return count($position);
		}
		/****************************************************************************
		 *                                                                          *
		 *   DON'T EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU WANT TO ACHIEVE  *
		 *                                                                          *
		 ****************************************************************************/
		
		public function jobExists($title){
			return $this->checkTitleExists('job', $title);
		}
		public function facilityExists($name){
			return $this->checkNameExists('facility', $name);
		}
}


/*********************************************
*
*      Execute!
*
*********************************************/

//ini_set('memory_limit','3000MB');


if (count($arg_files) != 1) {
		usage("Please specify the name of a spreadsheet to process");
}

reset($arg_files);
$file = current($arg_files);
if($file[0] == '/') {
		$file = realpath($file);
} else {
		$file = realpath($dir. '/' . $file);
}
if (!is_readable($file)) {
		usage("Please specify the name of a spreadsheet to import: " . $file . " is not readable");
}

I2CE::raiseMessage("Loading from $file");


$processor = new PositionData_Import($file);
$processor->run();

echo "Processing Statistics:\n";
print_r( $processor->getStats());


# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
