#!/bin/bash
echo "call ihris-update"
php /var/lib/iHRIS/sites/ihris-manage-site/pages/index.php --update=1

echo "Restarting memcached server"
sudo service memcached restart

echo "Restarting apache2 server"
sudo service apache2 restart